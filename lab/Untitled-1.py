import re
myfile = open('input.txt', 'r').read()
temp = re.findall(r'[-+]?\d+', myfile)
s1 = temp[::-1]
res = ' '.join(s1)
print(res)
f = open('output.txt', 'w')
f.write(res)
f.close()